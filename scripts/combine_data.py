#!/bin/python3
import os
import re
from itertools import groupby
from operator import itemgetter
from typing import Any, Dict, List


def read_file(path: str) -> List[Dict[str, Any]]:
    with open(path, "r") as f:
        cols = f.readline().strip().split(",")
        data = [
            {col: val for col, val in zip(cols, line.strip().split(","))}
            for line in f
        ]

    if "bishop" in path:
        data = [
            d
            for d in data
            if "professor" in d["role"].lower()
            or "faculty" in d["role"].lower()
            or "intructor" in d["role"].lower()
        ]

    if "campus" in cols:
        data = [
            {
                **d,
                **{
                    "city": re.match(
                        r"^ENAP (?:à|en) (\w+)$", d["campus"]
                    ).group(1)
                    if d["campus"] != "Aucun"
                    else ""
                },
            }
            for d in data
        ]
    elif "city" not in cols:
        uni2city = {
            "Bishop university": "Sherbrooke",
            "École de technologie supérieure": "Montréal",
            "HEC Montréal": "Montréal",
            "Université TÉLUQ": "",
            "Université du Québec à Montréal": "Montréal",
            "Institut national de la recherche scientifique": "Montréal",
            "Polytechnique": "Montréal",
        }
        data = [{**d, **{"city": uni2city[d["university"]]}} for d in data]

    if "sex" in cols:
        data = [
            {
                **d,
                **{
                    "sex": "m"
                    if d["sex"] == "m" or d["sex"] == "homme"
                    else "f"
                    if d["sex"] == "f" or d["sex"] == "femme"
                    else "u"
                },
            }
            for d in data
        ]
    else:
        data = [{**d, **{"sex": "u"}} for d in data]

    if "expertises" in cols:
        data = [
            {
                **{key: val for key, val in d.items() if key != "expertises"},
                **{
                    "sub_expertise": re.sub(
                        r"[\"'\[\]]", "", expertise
                    ).lower()
                },
            }
            for d in data
            for expertise in re.split(r"\s*\|\s*", d["expertises"])
            if expertise or not d["expertises"]
        ]
    elif "sub_expertise" not in cols:
        data = [{**d, **{"sub_expertise": ""}} for d in data]

    if "expertise" in cols:
        data = [
            {**d, **{"expertise": d["expertise"].lower().strip()}}
            for d in data
        ]
    elif "department" in cols:
        data = [
            {
                **d,
                **{
                    "expertise": re.sub(
                        r"^(?:[Éé]cole des|[Dd]épartement(?: des?)?)\s?(?:d')?",  # noqa
                        "",
                        d["department"].replace('"', ""),
                    )
                    .lower()
                    .strip()
                },
            }
            for d in data
        ]
    else:
        data = [{**d, **{"expertise": ""}} for d in data]

    return data


def read_files() -> List[List[Dict[str, Any]]]:
    path = os.path.join(os.path.dirname(__file__), os.pardir, "data")
    return [
        read_file(os.path.join(path, file_))
        for file_ in os.listdir(path)
        if "ethnicity" not in file_
        and "diversity" not in file_
        and "all" not in file_
        and "grouped" not in file_
        and "expertises" not in file_
        and "distribution" not in file_
        and not file_.endswith("ipynb")
    ]


def combine_files(data: List[List[Dict[str, Any]]]) -> List[Dict[str, Any]]:
    wanted_fields = [
        "name",
        "sex",
        "expertise",
        "sub_expertise",
        "email",
        "number",
        "university",
        "city",
        "department",
    ]
    return [
        {field: d.get(field, "").replace('"', "") for field in wanted_fields}
        for university in data
        for d in university
    ]


def save(data: List[Dict[str, Any]]) -> None:
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "all.csv"
    )
    cols = list(data[0].keys())
    with open(path, "w") as f:
        f.write(f"{','.join(cols)}\n")
        for d in data:
            f.write(f"{','.join(d.values())}\n")


def group_by_data(data: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    fields = ["name", "expertise", "university", "city", "sex"]
    return [
        {
            **{field: val for field, val in zip(reversed(fields), group[0])},
            **{"count": len(list(group[1]))},
        }
        for group in groupby(
            (
                {field: val for field, val in zip(reversed(fields), group[0])}
                for group in groupby(
                    sorted(data, key=itemgetter(*reversed(fields))),
                    key=itemgetter(*reversed(fields)),
                )
            ),
            key=itemgetter(*reversed(fields[1:])),
        )
    ]


def save_grouped_data(data: List[Dict[str, Any]]) -> None:
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "grouped.csv"
    )
    cols = list(data[0].keys())
    with open(path, "w") as f:
        f.write(f"{','.join(cols)}\n")
        for d in data:
            if d["expertise"]:
                f.write(f"{','.join(str(d[col]) for col in cols)}\n")


def create_expertises_file(grouped: List[Dict[str, Any]]) -> None:
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "expertises.csv"
    )
    if os.path.exists(path):
        with open(path, "r") as f:
            cols = f.readline().strip().split(",")
            expertises = [
                {col: val for col, val in zip(cols, line.strip().split(","))}
                for line in f
            ]
    else:
        expertises = []

    expertises_ = list(sorted(set(d["expertise"] for d in grouped)))

    if len(expertises) != len(expertises_):
        expertises = {e["expertise"]: e["domain"] for e in expertises}
        expertises = [
            {"expertise": e, "domain": expertises.get(e, "")}
            for e in expertises_
        ]
    with open(path, "w") as f:
        f.write("expertise,domain\n")
        for e in expertises:
            f.write(f"{e['expertise']},{e['domain']}\n")


def create_distribution_file(grouped) -> None:
    expertise_path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "expertises_final.csv"
    )
    try:
        with open(expertise_path, "r") as f:
            cols = f.readline().strip().split(",")
            expertises = [
                {
                    field: val
                    for field, val in zip(cols, line.strip().split(","))
                }
                for line in f
            ]
    except FileNotFoundError:
        raise FileNotFoundError(
            "You must copy the finished expertises.csv file to "
            "expertises_final.csv"
        )
    distribution_path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "distribution.csv"
    )

    expertise2domain = {e["expertise"]: e["domain"] for e in expertises}
    data = [
        {**g, **{"domain": expertise2domain[g["expertise"]]}}
        for g in grouped
        if g["expertise"] in expertise2domain
    ]

    cols = list(data[0].keys())
    with open(distribution_path, "w") as f:
        f.write(f"{','.join(cols)}\n")
        for d in data:
            f.write(f"{','.join(str(d[col]) for col in cols)}\n")


def read_diversity_data() -> Dict[str, Dict[str, str]]:
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "final_ethnicity.csv"
    )
    with open(path, "r") as f:
        cols = f.readline().strip().split(",")
        data = [
            {col: val for col, val in zip(cols, line.strip().split(","))}
            for line in f
        ]
        return {
            name: {
                f: v
                for f, v in next(group).items()
                if f == "ethnicity" or f == "sex"
            }
            for name, group in groupby(
                sorted(data, key=itemgetter("name")), key=itemgetter("name")
            )
        }


def read_expertise_data() -> Dict[str, Dict[str, str]]:
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "expertises_final.csv"
    )
    with open(path, "r") as f:
        cols = f.readline().strip().split(",")
        data = [
            {col: val for col, val in zip(cols, line.strip().split(","))}
            for line in f
        ]
    return {d["expertise"]: d for d in data}


def save_complete(
    data: List[Dict[str, Any]],
    diversity: Dict[str, Any],
    expertises: Dict[str, str],
) -> None:
    data = [
        {
            **d,
            **{
                "sex": diversity[d["name"]].get("sex", d["sex"])
                if d["name"] in diversity
                else d["sex"],
                "ethnicity": diversity[d["name"]].get("ethnicity", "UN")
                if d["name"] in diversity
                and len(diversity[d["name"]].get("ethnicity", "UN")) == 2
                else "UN",
                "domain": abb_to_domain(expertises[d["expertise"]]["domain"]),
            },
        }
        for d in data
        if d["expertise"] in expertises
    ]
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "complete.csv"
    )
    cols = list(data[0].keys())
    with open(path, "w") as f:
        f.write(f"{','.join(cols)}\n")
        for d in data:
            f.write(f"{','.join(d[col] for col in cols)}\n")


def abb_to_domain(domain):
    return {
        "app": "Sciences appliquées",
        "art": "Arts",
        "aut": "Autre",
        "bus": "Affaires",
        "dro": "Droit",
        "edu": "Éducation",
        "let": "Lettres",
        "pur": "Sciences pures",
        "san": "Sciences santé",
        "soc": "Sciences sociales",
    }[domain]


if __name__ == "__main__":
    data = read_files()
    combined = combine_files(data)
    save(combined)
    grouped = group_by_data(combined)
    save_grouped_data(grouped)
    create_expertises_file(grouped)
    create_distribution_file(grouped)
    diversity = read_diversity_data()
    expertises = read_expertise_data()
    save_complete(combined, diversity, expertises)
